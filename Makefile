default:
	pdflatex -shell-escape *.tex
	pdflatex -shell-escape *.tex

upload_tobast:
	scp slides.pdf www.tobast:~/tobast.fr/public_html/m2/graphics/jelly.pdf
